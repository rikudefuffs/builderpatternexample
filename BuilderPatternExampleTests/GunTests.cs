using Microsoft.VisualStudio.TestTools.UnitTesting;
using BuilderPatternExample.Builders;
using BuilderPatternExample.Data;

namespace BuilderPatternExampleTests
{
    [TestClass]
    public class GunTests
    {
        [TestMethod]
        public void Shoot_HasAmmo_AmmoCountIsReducedByOne()
        {
            Magazine magazine = A.Magazine.WithAmmo(AmmoType.TenGaugeShells)
                                          .WithCapacity(10)
                                          .WithCurrentAmmo(10);
            Gun gun = A.Gun.WithMagazine(magazine);
            int ammoBeforeShooting = gun.magazine.currentAmmo;
            gun.Shoot();
            Assert.AreEqual(ammoBeforeShooting, gun.magazine.currentAmmo + 1);
        }

        [TestMethod]
        public void Shoot_HasNoAmmo_DoesNothing()
        {
            Magazine magazine = A.Magazine.WithAmmo(AmmoType.TenGaugeShells)
                                          .WithCapacity(10)
                                          .WithCurrentAmmo(0);
            Gun gun = A.Gun.WithMagazine(magazine);
            int ammoBeforeShooting = gun.magazine.currentAmmo;
            gun.Shoot();
            Assert.AreEqual(ammoBeforeShooting, gun.magazine.currentAmmo);
        }
    }
}
