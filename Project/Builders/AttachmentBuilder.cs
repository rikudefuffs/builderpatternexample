﻿using BuilderPatternExample.Data;

namespace BuilderPatternExample.Builders
{
    public class AttachmentBuilder
    {
        string name;

        public AttachmentBuilder WithName(string name)
        {
            this.name = name;
            return this;
        }

        public Attachment Build()
        {
            return new Attachment(name);
        }

        public static implicit operator Attachment(AttachmentBuilder builder)
        {
            return builder.Build();
        }
    }
}
