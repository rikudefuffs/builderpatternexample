﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Builders
{
    /// <summary>
    /// Syntactic sugar for the builders
    /// </summary>
    public class A
    {
        public static GunBuilder Gun { get { return new GunBuilder(); } }
        public static MagazineBuilder Magazine { get { return new MagazineBuilder(); } }
    }

    public class An
    {
        public static AttachmentBuilder Attachment { get { return new AttachmentBuilder(); } }
    }
}
