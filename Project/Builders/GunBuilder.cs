﻿using BuilderPatternExample.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Builders
{
    public class GunBuilder
    {
        string name;
        Magazine magazine;
        Attachment[] attachments;

        /// <summary>
        /// Adds a specific magazine to the builder, so it can be injected in the built instance of the Gun
        /// </summary>
        /// <param name="magazine"></param>
        /// <returns>The builder, so you can chain With<X> methods</returns>
        public GunBuilder WithMagazine(Magazine magazine)
        {
            this.magazine = magazine;
            return this;
        }

        /// <summary>
        /// Adds a specific attachments to the builder, so it can be injected in the built instance of the Gun
        /// </summary>
        /// <param name="attachments"></param>
        /// <returns>The builder, so you can chain With<X> methods</returns>
        public GunBuilder WithAttachments(Attachment[] attachments)
        {
            this.attachments = attachments;
            return this;
        }

        public GunBuilder WithName(string name)
        {
            this.name = name;
            return this;
        }

        /// <summary>
        /// Generates an instance of the Gun, initialized according to the previously injected data
        /// </summary>
        /// <returns></returns>
        public Gun Build()
        {
            return new Gun(name, magazine, attachments);
        }

        /// <summary>
        /// Automatically converts the builder to a Gun when you assing it to a Gun variable
        /// I.E: Gun gun = A.GunBuilder.WithMagazine(magazine);
        /// </summary>
        /// <param name="builder"></param>
        public static implicit operator Gun(GunBuilder builder)
        {
            return builder.Build();
        }
    }
}
