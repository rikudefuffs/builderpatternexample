﻿using BuilderPatternExample.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Builders
{
    public class MagazineBuilder
    {
        int capacity;
        AmmoType ammoType;
        int currentAmmo;

        public MagazineBuilder WithCurrentAmmo(int currentAmmo)
        {
            this.currentAmmo = Math.Clamp(currentAmmo, 0, capacity);
            return this;
        }

        public MagazineBuilder WithCapacity(int capacity)
        {
            this.capacity = capacity;
            return this;
        }

        public MagazineBuilder WithAmmo(AmmoType ammoType)
        {
            this.ammoType = ammoType;
            return this;
        }

        public Magazine Build()
        {
            return new Magazine(capacity, ammoType, currentAmmo);
        }

        public static implicit operator Magazine(MagazineBuilder builder)
        {
            return builder.Build();
        }
    }
}
