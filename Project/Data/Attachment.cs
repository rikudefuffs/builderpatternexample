﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Data
{
    /// <summary>
    /// Something that can be added to a Gun to improve its efficiency/add extra functions
    /// I.E: A laser sight, a scope, a silencer
    /// </summary>
    public class Attachment
    {
        public string name;

        public Attachment(string name)
        {
            this.name = name;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
