﻿using System.Linq;


namespace BuilderPatternExample.Data
{
    /// <summary>
    /// A weapon that can fire projectiles
    /// </summary>
    public class Gun
    {
        string name;
        public Magazine magazine { get; private set; }
        Attachment[] attachments;

        public Gun(string name, Magazine magazine, Attachment[] attachments)
        {
            this.name = name;
            this.magazine = magazine;
            this.attachments = attachments;
        }

        public void Reload()
        {
            magazine.Reload();
        }

        public void Shoot()
        {
            if (magazine.currentAmmo < 1) { return; }
            magazine.currentAmmo--;
        }

        public override string ToString()
        {
            string attachmentsDescription = (attachments == null) ? "None" : string.Join(", ", attachments.Select(a => a.ToString()));
            return string.Format
            (
                 "Weapon name: {0}\nMagazine: {1}\nAttachments: {2}\n",
                 name, magazine.ToString(), attachmentsDescription
            );
        }
    }
}
