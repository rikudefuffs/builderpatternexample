﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPatternExample.Data
{
    public enum AmmoType
    {
        NineMillimeters,
        TenGaugeShells,
        TwelveGaugeShells
    }

    /// <summary>
    /// Holds the ammo of a Gun
    /// </summary>
    public class Magazine
    {
        int capacity;
        AmmoType ammoType;
        public int currentAmmo;

        public Magazine(int capacity, AmmoType ammoType, int currentAmmo)
        {
            this.capacity = capacity;
            this.ammoType = ammoType;
            this.currentAmmo = currentAmmo;
        }

        public void Reload()
        {
            currentAmmo = capacity;
        }

        public override string ToString()
        {
            return string.Format("{0} caliber magazine [Ammo: {1}/{2}]", ammoType, currentAmmo,capacity);
        }
    }
}
