﻿/*
 * The "Builder Pattern" is a pattern that allows to clean up the code
 * needed to instantiate an object that has to be initialized in some way,
 * that can have multiple constructors or that relies on other objects to work
 * 
 * Usage:
 * - Create a builder for the object to isntantiate (I.E: a GunBuilder for a Gun)
 * - Within the builder, create a With<X>() method for each attribute (I.E: ammo amount). 
 *   This method initializes the attribute and returns the builder (so you can chain methods)
 * - Within the builder, create a Build() method which returns a new object initialized with all the attributes needed
 * - Add some syntactic sugar (see "Builders" class and sublcasses A and AN)
 */

using System;
using BuilderPatternExample.Builders;
using BuilderPatternExample.Data;

namespace BuilderPatternExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Attachment laserSight = An.Attachment.WithName("Laser Sight");
            Attachment silencer = An.Attachment.WithName("Silencer");
            Attachment[] attachments = new Attachment[] { laserSight, silencer };

            Magazine magazine = A.Magazine.WithCapacity(15)
                                          .WithAmmo(AmmoType.NineMillimeters);
            Gun gun = A.Gun.WithName("Beretta M0192")
                           .WithMagazine(magazine)
                           .WithAttachments(attachments);
            gun.Reload();
            Console.WriteLine(gun);
        }
    }
}
