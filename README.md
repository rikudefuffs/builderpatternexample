## Details
/*
 * The "Builder Pattern" is a pattern that allows to clean up the code
 * needed to instantiate an object that has to be initialized in some way,
 * that can have multiple constructors or that relies on other objects to work
 * 
 * Usage:
 * - Create a builder for the object to isntantiate (I.E: a GunBuilder for a Gun)
 * - Within the builder, create a With<X>() method for each attribute (I.E: ammo amount). 
 *   This method initializes the attribute and returns the builder (so you can chain methods)
 * - Within the builder, create a Build() method which returns a new object initialized with all the attributes needed
 * - Add some syntactic sugar (see "Builders" class and sublcasses A and AN)
 */
